<?php include_once('_inc/header.php'); ?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Google Map</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<div class="row">
		<form action="admin_map.php" method="get" class="col-md-6" id="map">
			<fieldset>
				<div class="form-group">
					<label>adresse</label>
					<input id="address" class="form-control">
				</div>
				<button type="submit" class="btn btn-block btn-primary">Géocoder</button>
			</fieldset>
		</form>
		<div class="col-md-6">
			<div id="map-display"></div>
		</div>
	</div>
	<!-- /.row -->
</div>
<!-- /#page-wrapper -->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPx6ASBHt1PsjI5ERruR-RC-9HMWiGmtk"></script>

<?php include_once('_inc/footer.php'); ?>
