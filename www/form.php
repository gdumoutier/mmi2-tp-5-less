<?php include_once('_inc/header.php'); ?>

	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Formulaire</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<form action="admin_form_ajax.php" method="get" class="col-md-6" id="preview">
				<fieldset>
					<legend>Édition</legend>
					<div class="form-group">
						<label for="title">Titre</label>
						<input id="title" type="text" name="title" required class="form-control">
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="a">A</label>
								<input id="a" name="a" type="number" required class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="b">B</label>
								<input id="b" name="b" type="number" required class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="content">Contenu</label>
						<textarea id="content" name="content" required class="form-control">Contenu</textarea>
					</div>
					<div class="form-group">
						<label for="type">Type de contenu</label>
						<div class="radio">
							<label>
								<input name="type" type="radio" required value="Page">Page
							</label>
						</div>
						<div class="radio">
							<label>
								<input name="type" type="radio" required value="Article">Article
							</label>
						</div>
						<div class="radio">
							<label>
								<input name="type" type="radio" required value="Actu">Actu
							</label>
						</div>
					</div>
					<button type="submit" class="btn btn-block btn-primary">Aperçu</button>
				</fieldset>
			</form>
			<div class="col-md-6">
				<fieldset>
					<legend>Previsualisation</legend>
					<div class="panel panel-default">
						<div class="panel-heading">
							Titre
						</div>
						<div class="panel-body">
							<p>Contenu</p>
						</div>
						<div class="panel-footer">
							Type de contenu
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		<!-- /.row -->
	</div>
	<!-- /#page-wrapper -->

<?php include_once('_inc/footer.php'); ?>
