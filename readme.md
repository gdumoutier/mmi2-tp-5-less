#TP : LessCSS

- Editez le fichier **admin.less** pour qu'il fasse appel à la bibliothèque CSS Bootstrap (version less)
- Après bootstrap, faites appel au fichier **sb-admin-2.less**
- Créez une fonction less **panel-color()**. Celle-ci doit accepter deux paramètres : **@name** et **@color**
	* Cette mixin doit créer un code similaire à celui de **.panel-primary**
	* Le nom de la classe créée doit reprendre le paramètres @name.
	* Le couleur du panel doit reprendre le paramètre @color.
- Réecrire une variable Bootstrap dans **admin.less** pour changer la police des titres par une police avec serif.
- A l'aide des fichiers less de Boostrap, trouvez comment ajouter un nouveau type d'alerte : **.alert-waiting**, de couleur **violette**.
- Elaborez une mixin **gradient()** pour créer un dégradé de couleur **@startColor** vers une couleur **@endColor**.
- Utilisez **gradient()** avec **panel-color()** pour faire un dégradé de **@color**, vers **@color** en plus foncé de 15%. 

## Bonus

- Créer votre propre classe **.button-@{size}** à l'aide d'une mixin.
- La mixin prendra en compte trois tailles : 
	**@small** : .85em 
	**@medium** : 1em
	**@large** 1.15em
- padding: .4em .6em;
- Chaque taille doit exprimer un background-color différent.
- À l'aide de http://lesscss.org/3.x/features/#css-guards-feature, si la taille et supérieur à 1em : doubler les paddings
- Le hover exprimera un background-color plus foncé de 10%
- Essayer les fonctions less mix() et spin().